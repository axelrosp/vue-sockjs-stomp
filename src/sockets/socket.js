import SockJS from "sockjs-client";
import Stomp from "webstomp-client";

class Socket {
  connected = false

  connect(callback) {
    this.socket = new SockJS("http://localhost:8080/pizza-fun");
    this.stompClient = Stomp.over(this.socket);
    this.stompClient.connect(
      {},
      frame => {
        this.connected = true;
        console.log(frame);
        this.stompClient.subscribe("/topic/restaurant-topic/d11c6f80-5482-404c-a166-775517a40bd5", tick => {
          console.log(tick);
          callback(JSON.parse(tick.body).content);
        });
      },
      error => {
        console.log(error);
        this.connected = false;
      }
    );
  }

  send(message) {
    console.log("Send message:" + message);
    if (this.stompClient && this.stompClient.connected) {
      this.stompClient.send("/app/hello", message, {});
    }
  }

  disconnect() {
    if (this.stompClient) {
      this.stompClient.disconnect();
    }
    this.connected = false;
  }

  tickleConnection() {
    this.connected ? this.disconnect() : this.connect();
  }

}

export default Socket

